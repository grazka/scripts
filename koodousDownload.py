import requests, urllib
import json


shaList=[]
while len(shaList)<50:

    r = requests.get(url="https://api.koodous.com/apks")
    d = r.json()

    for i in d['results']:

        shaList.append(i['sha256'])
        print("Building list " + i['sha256'])

l = set(shaList)
for i in l:
    sha256 = i
    
    url_koodous = "https://api.koodous.com/apks/%s/download" % sha256

    r = requests.get(url=url_koodous, headers={'Authorization': 'Token a01aedccd3bddd683f4be2851f71d1e0b42efdf7'})

    
    outpath = "/home/tron/apps/"+i+".apk"
    
    if r.status_code is 200:
    #    testfile = urllib.URLopener()
        urllib.urlretrieve(r.json().get('download_url'), outpath)
        print("downloading: " + i)

print("Done")
